$("document").ready(function($){
 

// navbar toggle js
$('.navbar-toggle').click(function() {
  $(this).toggleClass('active');
});

//footer toggle
$('.footer-widget h4').click(function(event) {
     if($(window).width()<481){
    $(this).toggleClass('on');
    $(this).next('ul').slideToggle();
    $(".footer-widget h4").not(this).next('ul').slideUp();
    $(".footer-widget h4").not(this).removeClass("on");

}

});


//navigation toggle
$('.navbar-nav li.drop-down > a').click(function(event) {
     if($(window).width()<768){
    event.preventDefault();
    $(this).toggleClass('on');
    $('.sub-menu').slideToggle();
}
});


//testimonial carousel
 $("#billboard-sliders").owlCarousel({
    nav: false,
    dots: true,
    loop: true,
    autoPlay:true,
    autoPlaySpeed: 3000,
    items: 1,
      itemsDesktop : false,
      itemsDesktopSmall : false,
      itemsTablet: false,
      itemsMobile : false,
responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
  });

      
});
