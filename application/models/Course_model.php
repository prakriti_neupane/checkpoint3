<?php
class Course_model extends CI_Model
{

	function get_all_course()
	{
		$result= $this->db->get('course_details');
		return $result->result_array();
	}

	function add_user($params)
	{
	    $this->db->insert('user', $params);
		return $this->db->insert_id();
	}


	function delete_user($userid)
	{
		$response= $this->db->delete('user', array('user_id'=> $userId));
		if($response){
			echo "data deleted successfully";
		}else{
			echo "error";
		}
	}

	function update_user($userid ,$params)
	{
		$this->db->where('user_id', $userid);
		$response= $this->db->update('user', $params);
		if($response){
			return "user updated successfully";
		}
		else
		{
			return "error occured updating";
		}


	}


	function add_courses()
	{
		$params = array(
				'username' => $this->input->post('username'),
	    		'password' => $this->input->post('password'), 
	    		'fullname'=> $this->input->post('full_name'),
	    		'email' => $this->input->post('email'),
	    		'address' => $this->input->post('address'),
	    		'phone_number' => $this->input->post('number'),
            );
        $this->db->insert('sc_user',$params);
        return $this->db->insert_id();
	}


	function get_course_materials()
	{
		$result= $this->db->get('course_materials');
		return $result->result_array();

	}


	public function removeCourses($id){
      $this->db->where("course_id", $id);
      $this->db->delete('course_details');
      if($this->db->affected_rows() > 0){
         return true;
      }else{
         return false;
      }
   }



   




}


 ?>