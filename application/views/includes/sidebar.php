<body class="">
    <div class="wrapper ">
        <div class="sidebar" data-color="orange">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
            <div class="logo">
                <a href="index.php" class="simple-text logo-mini">
                    AD
                </a>
                <a href="index.php" class="simple-text logo-normal">
                    Admin Dashboard
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li>
                        <a href="<?php echo base_url() ?>/index.php/pages/adminDashboard">
                            <i class="now-ui-icons design_app"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>

                    <li>
                        <a href="<?php echo base_url() ?>/index.php/Course/addCourses">
                            <i class="now-ui-icons ui-1_bell-53"></i>
                            <p>Add new course</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>/index.php/course/listCourses">
                            <i class="now-ui-icons design_bullet-list-67"></i>
                            <p>Available Courses</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>