<!-- footer-section -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="ftr-logo">
                    <img src="images/logo_trans.png" alt="">
                </div>
            </div>
            <div class="col-md-3  col-sm-3  col-xs-12  visible">
                <div class="footer-widget">
                    
                    <div class="footer-contacts">
                        <p>Online Study Portal <br>
                        For easy and faster learning</p>
                        <p class="copyright">© 2018 Online Study Portal<br> All Rights Reserved. </p>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-4">
                <div class="footer-widget">
                    <h4>Quick Links <span class="dropdown"><i class="fa fa-angle-down"></i></span></h4>
                    <ul>
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3  col-xs-4">
                <div class="footer-widget">
                    <h4>Available Courses<span class="dropdown"><i class="fa fa-angle-down"></i></span></h4>
                    <ul>
                        <li><a href="#">Javascript</a></li>
                        <li><a href="#">Python</a></li>
                        <li><a href="#">Bootstrap</a></li>
                        <li><a href="#">PHP</a></li>
                    </ul>
                </div>
            </div>
             <div class="col-md-3  col-sm-3  col-xs-12  hidden">
                <div class="footer-widget">
                    
                    <div class="footer-contacts">
                        <p>Online Study 
                        Portal for students</p>
                        <p class="copyright">© 2018 Online Study Portal<span> All Rights Reserved. </span></p>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</footer>

<!--bootstrap-core-js-->
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/owl.carousel.min.js')?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
baguetteBox.run('.gallery-dynamo');
</script>
<script type="text/javascript" src="<?php echo base_url('assets/js/custom.js')?>"></script>
</body>
</html>