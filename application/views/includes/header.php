<!DOCTYPE html>
<html lang="en-us">
  <head>
    <title>Online Class</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <link rel="icon" type="image/png" id="favicon" href="images/favicon.gif"/>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css')?>">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700,900" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/prettyPhoto.css')?>" rel="stylesheet">
    <!-- Owl Carousel Css -->
    <link href="<?php echo base_url('assets/css/owl.carousel.css')?>" rel="stylesheet"/> <!-- Core Owl Carousel CSS File  * v1.3.3 -->
    <!-- <link href="css/freshtone.min.css" rel="stylesheet" type="text/css"/> -->
    <link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/media.css')?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://use.typekit.net/tpk0tiu.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css" rel="stylesheet" type="text/css">
<link href="http://dfwsunroomsandpatios.com/css/fluid-gallery.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <header class="mt-header">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-3">
            <div class="logo">
              <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/images/logo.jpg')?>" alt="online-logo" /></a>
            </div>
          </div>
          <div class="col-md-9 col-sm-9">
            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar mt-1"></span>
                  <span class="icon-bar mt-2"></span>
                  <span class="icon-bar mt-3"></span>
                  </button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    <li class="parent dropdown home-col"> <a href="<?php echo base_url(); ?>" <?php if(@$menuSelected == 'home') echo 'class="active"'; ?>>  Home</a> </li>
                    <li class="full-width parent"> <a href="#" <?php if(@$menuSelected == 'about') echo 'class="active"'; ?>> About Us </a> </li>
                    <li class="drop-down"> <a href="service.php" <?php if(@$menuSelected == 'service') echo 'class="active"'; ?>>Available Courses <span class="arrow"></span></a>
                      <ul class="sub-menu">
                        <li><a href="#">Javascript</a></li>
                        <li><a href="#">Python</a></li>
                        <li><a href="#">Bootstrap</a></li>
                        <li><a href="#">PHP</a></li>
                      </ul>

                     </li>
                    <li class="parent"> <a href="<?php echo base_url(); ?>index.php/pages/membership" <?php if(@$menuSelected == 'packaging') echo 'class="active"'; ?>>Join Us</a> </li>
                    <li class="full-width parent"> <a href="#" <?php if(@$menuSelected == 'gallery') echo 'class="active"'; ?>>Contact Us</a> </li>
            
                    <li class="phone"> <a href="<?php echo base_url(); ?>index.php/admin/showAdminForm">Admin Login</a>  </li>
                  </ul>
                </div>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </header>

    


  </div>
</div>
<body class="">
    <div class="wrapper ">