<?php 

?>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute bg-primary fixed-top">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-toggle">
                            <button type="button" class="navbar-toggler">
                                <span class="navbar-toggler-bar bar1"></span>
                                <span class="navbar-toggler-bar bar2"></span>
                                <span class="navbar-toggler-bar bar3"></span>
                            </button>
                        </div>
                        <a class="navbar-brand" href="#pablo">Dashboard</a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <i class="now-ui-icons users_single-02"></i>
                                  <p>
                                    <span class="d-lg-none d-md-block">Some Actions</span>
                                </p>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                     <a class="dropdown-item" href="../index.php">Visit Site</a>
                                      <a class="dropdown-item" id= "logout" href="includes/signOut.php">Sign Out</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="panel-header panel-header-lg">
                    <canvas id="bigDashboardChart"></canvas>
                </div>
                <div class="content">
                    <div class="row">
                        <?php
                        // include('../controller/adminController.php');
                        // $recentBook= new adminController();
                        // $recentBook->  settableName('book_details');
                        // $select= $recentBook->recentItem();
                        // if($select->num_rows > 0){
                        //    while($row= $select->fetch_assoc()){
                              ?>
                              <div class="col-lg-4 col-md-6">
                                <div class="card card-chart">
                                    <div class="card-header">
                                        <h5 class="card-category"><?php //echo $row['author'] ?></h5>
                                        <h4 class="card-title"><?php// echo $row['book_name']; ?></h4>
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                                                <i class="now-ui-icons loader_gear"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="bookList.php">Update</a>
                                                <a class="dropdown-item text-danger" href="bookList.php">Remove Data</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="chart-area">
                                            <canvas id="lineChartExampleWithNumbersAndGrid"></canvas>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <i class="now-ui-icons arrows-1_refresh-69"></i> Just Updated
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                      //        }
                      //   }else{
                      //     echo "No record found here";
                      // }
                      ?>
                      <a href="bookList.php"><button  type="button" class="btn btn-primary">View All Books</button></a>
                  </div>
              </div>
              <div class="row">

                  <div class="col-md-12">
                      <div class="card">
                        <div class="card-header">
                            <h5 class="card-category">Recently registered Users</h5>
                            <h4 class="card-title"> User Stats</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                              <table class="table">
                                <thead class=" text-primary">
                                  <th >
                                    User Id
                                </th>
                                <th >
                                    Fullname
                                </th>
                                <th >
                                    Username
                                </th>
                                <th>
                                    Email Address
                                </th>
                                <th >
                                    Address
                                </th>
                                <th >
                                    Phone Number
                                </th>
                            </thead>
                            <tbody>
                                <?php
                                // include('../controller/userController.php');
                                // $newUsers= new userController();
                                // $newUsers->  settableName('user');
                                // $select= $newUsers->recentAddedUser();
                                // if($select->num_rows > 0){
                                //    while($row= $select->fetch_assoc()){
                                      ?>
                                      <tr>
                                        <td>
                                         <?php //echo $row["user_id"] ?>
                                     </td>
                                     <td>
                                        <?php //echo $row["fullname"] ?>
                                    </td>
                                    <td>
                                       <?php //echo $row["username"] ?>
                                   </td>
                                   <td>
                                      <?php //echo $row["email"] ?>
                                  </td>
                                  <td >
                                      <?php //echo $row["address"] ?>
                                  </td>

                                  <td>
                                      <?php //echo $row["phone_number"] ?>
                                  </td>
                                  <tr>
                                     <?php 
                              //         }
                              //    }else{
                              //     echo "No record found here";
                              // }

                              ?>

                          </tbody>
                      </table>
                      <a href="fpdfData.php" target="_blank"><button  type="button" class="btn btn-primary">View All Users</button></a>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<?php //include('includes/footer.php') ?>