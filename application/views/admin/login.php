<!DOCTYPE html>
<html lang="en-us">
  <head>
    <title>Online Class</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <link rel="icon" type="image/png" id="favicon" href="images/favicon.gif"/>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css')?>">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700,900" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/prettyPhoto.css')?>" rel="stylesheet">
    <!-- Owl Carousel Css -->
    <link href="<?php echo base_url('assets/css/owl.carousel.css')?>" rel="stylesheet"/> <!-- Core Owl Carousel CSS File  * v1.3.3 -->
    <!-- <link href="css/freshtone.min.css" rel="stylesheet" type="text/css"/> -->
    <link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/media.css')?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://use.typekit.net/tpk0tiu.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css" rel="stylesheet" type="text/css">
<link href="http://dfwsunroomsandpatios.com/css/fluid-gallery.css" rel="stylesheet" type="text/css">
  </head>

  <div class= "admin-form">
<form id="contact-form" method="post" action="<?php echo base_url() ?>/index.php/admin/adminLogin" role="form">
		<div class="messages"><h2>Admin Login</h2></div>
		<div class="controls">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="form_name">Username</label>
						<input id="form_name" type="text" name="user" class="form-control" placeholder="Please enter your Username *" required="required" data-error="Username is required.">
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="form_lastname">Password</label>
						<input id="form_lastname" type="password" name= "pw" class="form-control" placeholder="Please enter your Password *" required="required" data-error="Password is required.">
						<div class="help-block with-errors"></div>
					</div>
				</div>

				<div class="col-md-12">
					<input type="submit" class="btn btn-success btn-send" name= "submit" value="Login">
				</div>
			</div>
		</div>
	</form>
</div>