<?php 
include('includes/header.php'); 
include('../includes/db_connection.php');


?>
<body class="">
    <div class="wrapper ">

        <?php include('includes/sidebar.php'); ?>
        <div class="container">
            <?php
            include('../controller/adminController.php');
            $db = new db_connection();
            $conn = $db->getConnection();
            if(isset($_GET['book_id']))
            {
                $selectId = new adminController();
                $selectId->settableName('book_details');
                $selectId->setbookId(trim($_GET['book_id']));
                $result = $selectId->getbyId();
                if($result->num_rows==1){
                    foreach($result as $value)
                    {
                        $id= $value['book_id'];
                        $name= $value['book_name'];
                        $price= $value['price'];
                        $writer= $value['author'];
                        $language= $value['language'];
                        $genre= $value['genre'];
                        $category_id= $value['category_id'];
                    }
                }
            }

            if(isset($_POST['update'])){
            // $id= mysqli_escape_string()($conn, $_POST['book_id']);
                $name= mysqli_real_escape_string($conn,$_POST['book_name']);
                $price= mysqli_real_escape_string($conn, $_POST['price']);
                $author= mysqli_real_escape_string($conn,$_POST['author']);
                $language= mysqli_real_escape_string($conn, $_POST['language']);
                $genre= mysqli_real_escape_string($conn, $_POST['genre']);
                $category= mysqli_real_escape_string($conn, $_POST['category']);



                $book_id= $_POST['book_id'];
                $book_name = $_POST['book_name'];
                $price = $_POST['price'];
                $author = $_POST['author'];
                $language = $_POST['language'];
                $genre = $_POST['genre'];
                $category_id =(int) ($_POST['category']);

                $updateBook = new adminController();
                $updateBook->settableName('book_details');
                $updateBook->setbookId($_POST['book_id']);
                $updateBook->setbookName($book_name);
                $updateBook->setPrice($price);
                $updateBook->setAuthor($author);
                $updateBook->setLanguage($language);
                $updateBook->setGenre($genre);
                $updateBook->setcatId($category_id);
                if(isset($book_image)){
                    $book_image = addslashes(file_get_contents($_FILES["imgfile"]["tmp_name"]));
                    $updateBook->setbookImage($book_image);
                }


                $UpdateQuery = $updateBook->updateData($_POST['book_id']);
                if($UpdateQuery){
                   echo "<script> window.alert('Data Updated successfully');
                   window.location.href = 'bookList.php';
                   </script>";

               }else{
                echo "<script>alert('Error to update Data')</script>";
            }
        }

        ?>
        <div class="main-panel">
         <div class="container">
            <div class="row main">
                <div class="main-login main-center">
                    <h5>Add New Book lists here</h5>
                    <form class="book-form" method="post"  enctype="multipart/form-data">

                      <div class="form-group">
                        <label for="bookname" class="cols-sm-2 control-label">Book ID</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-book fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="book_id" id="book_id" readonly="readonly" required="" value= "<?php echo $id;?>" placeholder="Enter Book  ID"/>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="bookname" class="cols-sm-2 control-label">Book Name</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-book fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="book_name" id="name" required="" value= "<?php echo $name; ?>"  placeholder="Enter Book Name"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="price" class="cols-sm-2 control-label">Price</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fas fa-dollar-sign" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="price" id="cost" value= "<?php echo $price ?>"required="" placeholder="Enter Price"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="authorname" class="cols-sm-2 control-label">Author</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fas fa-pencil-alt" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="author" id="writer" value= "<?php echo $writer ?>" required=""  placeholder="Enter Writer Name"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="language" class="cols-sm-2 control-label">Language</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fas fa-language" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="language" id="language" required="" value= "<?php echo $language?>"  placeholder="Language Type"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="genere" class="cols-sm-2 control-label">Genre</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fas fa-tags" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="genre" id="genre" value= "<?php echo $genre; ?>" required=""  placeholder="Genre of Book"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="genere" class="cols-sm-2 control-label">Book Category</label>
                        <select class="form-control" id="cat" name="category" required="">
                            <option disabled="" val="" selected="hidden">Choose Category</option>
                            <?php 
                            $selectCat= new adminController();
                            $selectCat-> settableName('book_category');
                            $result= $selectCat->fetchData();
                            if($result->num_rows>0){
                                while($row=$result->fetch_array())
                                {  
                                    if($row['category_id'] == $category_id){
                                        echo "<option value='$row[category_id]' selected>" .$row['category_name']. "</option>";
                                    }else{
                                        echo "<option value='$row[category_id]'>" .$row['category_name']. "</option>";
                                    }
                                }
                            }else{
                                echo "<option value= '' disabled='' selected='hidden'>No any category</option>";
                            }
                            ?>

                        </select>
                    </div>


                    <div class="form-group">
                        <label for="file" class="cols-sm-2 control-label">Upload book Image</label>
                        <div class="cols-sm-10">
                            <span class="btn btn-default btn-file">
                                Browse <input type="file" id="image" name="imgfile" accept=".jpg, .jpeg, .png">
                            </span>
                        </div>
                    </div>
                    <div class="container">

                        <div class="form-group ">
                            <button type="submit" id="update" name= "update" class="btn btn-primary btn-lg btn-block login-button">Update</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>



</div>
</div>
<?php 
include('includes/footer.php'); ?>

<script>
    $(document).ready(function(){
        $('#insert').click(function(){
         var image_name= $('#profile_pic').val();
         if(image_name== ''){
            alert("select image");
        }
        else{
            var extension= $('#image').val().split('.').pop().toLowerCase();
            if(jQuery.inArray(extension, ['png', 'jpg', 'jpeg']) == -1){
                alert('invalid image file');
                $('#image').val('');
                return false;
            }
        }

    });
    });

</script>