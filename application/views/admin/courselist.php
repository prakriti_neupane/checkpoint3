<?php 
//include('includes/header.php') ?>
<body class="">
    <div class="wrapper ">

        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute bg-primary fixed-top">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-toggle">
                            <button type="button" class="navbar-toggler">
                                <span class="navbar-toggler-bar bar1"></span>
                                <span class="navbar-toggler-bar bar2"></span>
                                <span class="navbar-toggler-bar bar3"></span>
                            </button>
                        </div>
                        <a class="navbar-brand" href="#pablo">Courses List</a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <form>
                            <div class="input-group no-border">
                                <input type="text" value="" class="form-control" id="searchHere" placeholder="Search...">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons ui-1_zoom-bold"></i>
                                </span>
                            </div>
                        </form>
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <i class="now-ui-icons users_single-02"></i>
                                  <p>
                                    <span class="d-lg-none d-md-block">Some Actions</span>
                                </p>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="../index.php">Visit Site</a>
                                <a class="dropdown-item" id= "logout" href="includes/signOut.php">Sign Out</a>

                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="panel-header panel-header-sm">
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Web Designing Courses</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                        <th>
                                            Course ID
                                        </th>
                                        <th>
                                            Course Name
                                        </th>
                                        <th>
                                            Course Description
                                        </th>
                                        <th>
                                            Course Image
                                        </th>

                                    </thead>
                                    <tbody id= "bookData">
                                       <?php 
                                       foreach ($course as $courses){

                                          ?>
                                          <tr>
                                            <td>
                                               <?php echo $courses["course_id"] ?>
                                           </td>
                                           <td>
                                            <?php echo $courses["course_name"] ?>
                                        </td>
                                        <td>
                                           <?php echo $courses["course_description"] ?>
                                       </td>
                                      <td class="text-right">
                                          <?php echo '<img src="data:image/jpeg;base64,'.base64_encode($courses['course_image'] ).'" height="100" width="200" class="img-thumnail" />';  ?>
                                      </td>
                                      <td><a href= "updateBook.php?book_id=<?php //echo $row['book_id']; ?>"><button>Edit</button></a></td>
                                      <td><a href="<?php echo base_url();?>index.php/Course/deleteCourse/?course_id= <?php echo $courses['course_id']; ?>" class= "delete_data" onclick= "return confirm('Are you sure you want to delete?');"><button style="color:white; background-color: red">Delete</button></a></td>

                                  </tr>

                                 <?php } ?>
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-12">
            <div class="card card-plain">
                <div class="card-header">
                    <h4 class="card-title">Web Development Courses</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                                    <thead class=" text-primary">
                                        <th>
                                            Course ID
                                        </th>
                                        <th>
                                            Course Name
                                        </th>
                                        <th>
                                            Course Description
                                        </th>
                                        <th>
                                            Course Image
                                        </th>

                                    </thead>
                                    <tbody id= "bookData">
                                       <?php 
                                       foreach ($course as $courses){

                                          ?>
                                          <tr>
                                            <td>
                                               <?php echo $courses["course_id"] ?>
                                           </td>
                                           <td>
                                            <?php echo $courses["course_name"] ?>
                                        </td>
                                        <td>
                                           <?php echo $courses["course_description"] ?>
                                       </td>
                                      <td class="text-right">
                                          <?php echo '<img src="data:image/jpeg;base64,'.base64_encode($courses['course_image'] ).'" height="100" width="200" class="img-thumnail" />';  ?>
                                      </td>
                                      <td><a href= "updateBook.php?book_id=<?php //echo $row['book_id']; ?>"><button>Edit</button></a></td>
                                      <td><a href= "deleteData.php?book_id=<?php //echo $row['book_id']; ?>"><button>Delete</button></a></td>

                                  </tr>

                                 <?php } ?>
                              </tbody>
                          </table>
        </div>
    </div>
</div>
</div>
</div>
</div>
<?php //include('includes/footer.php'); ?>
<script>
    $(document).ready(function(){
        $('#searchHere').on("keyup", function(){
            var value= $(this).val().toLowerCase();
            $("#bookData tr").filter(function(){
                $(this).toggle($(this). text().toLowerCase().indexOf(value) > -1)
            });

        });
    });

    $(function()
    {
        $('a#logout').click(function(){
          if(confirm('Are you sure to logout?')){
            return true;
        }
        return false;

    });

    });
</script>