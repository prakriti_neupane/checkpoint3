<?php 

// include('includes/header.php'); 
// include('../includes/db_connection.php');
?>
<body class="">
    <div class="wrapper ">

        <?php //include('includes/sidebar.php'); ?>
        
        <?php
        // include('../controller/adminController.php');
        // if(isset($_POST['additem'])){
        //     // $name= mysqli_real_escape_string($connect,$_POST['book_name']);
        //     // $price= mysqli_real_escape_string($_POST['price']);
        //     // $author= mysqli_real_escape_string($_POST['author']);
        //     // $language= mysqli_real_escape_string($_POST['language']);
        //     // $genre= mysqli_real_escape_string($_POST['genre']);
        //     // $category= mysqli_real_escape_string($_POST['category']);


        //     $book_name = $_POST['book_name'];
        //     $price = $_POST['price'];
        //     $author = $_POST['author'];
        //     $language = $_POST['language'];
        //     $genre = $_POST['genre'];
        //     $category_id =(int) ($_POST['category']);
        //     $book_image = addslashes(file_get_contents($_FILES["imgfile"]["tmp_name"]));

        //     $addBook = new adminController();
        //     $addBook->settableName('book_details');
        //     $addBook->setbookName($book_name);
        //     $addBook->setPrice($price);
        //     $addBook->setAuthor($author);
        //     $addBook->setbookImage($book_image);
        //     $addBook->setLanguage($language);
        //     $addBook->setGenre($genre);
        //     $addBook->setcatId($category_id);

        //     // $insert= "INSERT INTO book_details(book_name, price, author, language, book_image, genre, category_id) VALUES('$name', '$price', '$author', '$language', '$file', '$genre', '$category')";
            
        //     $addQuery = $addBook->insertBook();
        //       // var_dump($addQuery);
            
        //     if($addQuery==true){
        //         echo "<script>alert('data inserted')</script>";
        //         header("location: index.php");
                
        //     }else{
        //         echo "error to insert";
        //     }
        // }

        ?>
        
        <div class="main-panel">
         <div class="container">
            <div class="row main">
                <div class="main-login main-center">
                    <h5>Add New Course here</h5>
                    <form class="course-form" method="post"  enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="bookname" class="cols-sm-2 control-label">Course Name</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-book fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="course_name" id="name" required=""  placeholder="Enter Course Name"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="price" class="cols-sm-2 control-label">Course Description</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fas fa-dollar-sign" aria-hidden="true"></i></span>
                                   <textarea class= "form-control" name="desc" id="des" required="" placeholder="Enter Description"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="genere" class="cols-sm-2 control-label">Course Category</label>
                            <select class="form-control" id="cat" name="category" required="">
                                <option disabled="" val="" selected="hidden">Choose Category</option>
                                <?php 
                                // $selectCat= new adminController();
                                // $selectCat-> settableName('book_category');
                                // $result= $selectCat->fetchData();
                                // if($result->num_rows>0){
                                //     while($row=$result->fetch_array())
                                //     {  
                                //         echo "<option value='$row[category_id]'>" .$row['category_name']. "</option>";
                                //     }
                                // }else{
                                //     echo "<option value= '' disabled='' selected='hidden'>No any category</option>";
                                // }
                                ?>

                            </select>
                        </div>


                        <div class="form-group">
                            <label for="file" class="cols-sm-2 control-label">Upload course Image</label>
                            <div class="cols-sm-10">
                                <span class="btn btn-default btn-file">
                                    Browse <input type="file" id="image" name="imgfile" required="" accept=".jpg, .jpeg, .png">
                                </span>
                            </div>
                        </div>


                        <div class="form-group ">
                            <button type="submit" id="insert" name= "additem" class="btn btn-primary btn-lg btn-block login-button">Add Item</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>



    </div>
</div>
<?php 
//include('includes/footer.php'); ?>

<script>
    $(document).ready(function(){
        $('#insert').click(function(){
         var image_name= $('#profile_pic').val();
         if(image_name== ''){
            alert("select image");
        }
        else{
            var extension= $('#image').val().split('.').pop().toLowerCase();
            if(jQuery.inArray(extension, ['png', 'jpg', 'jpeg']) == -1){
                alert('invalid image file');
                $('#image').val('');
                return false;
            }
        }

    });
    });

</script>