<?php
// foreach ($user as $u) 
// {
// 	echo $u['user_id'];
// 	echo '</br>'. $u['username'];
// 	echo '<br>'. $u['fullname'];
// }
?>


<?php
$menuSelected = 'home';
 ?>
<!--banner-container-starts-here-->
<section id="billboard-container">
  <div id="billboard-sliders">
    <div class="item">
      <figure>
        <img src="<?php echo base_url('assets/images/banner2.jpg')?>" alt="" />
      </figure>
      <div class="container">
        <div class="banner-caption">
          <h1>Growing Compassionate young leaders through service Learning</h1>
          <p>A free oline course platform</p>
        </div>
      </div>
    </div>
    <div class="item">
      <figure>
        <img src="<?php echo base_url('assets/images/banner1.jpg')?>" alt="" />
      </figure>
      <div class="container">
        <div class="banner-caption">
           <h1>Growing Compassionate young leaders through service Learning</h1>
          <p>A free oline course platform</p>d for less money.</p>
        </div>
      </div>
    </div>
    <div class="item">
      <figure>
        <img src="<?php echo base_url('assets/images/banner3.jpg')?>" alt="" />
      </figure>
      <div class="container">
        <div class="banner-caption">
          <h1>Custom Quality Bathroom Renovations in Sydney</h1>
          <p>Elegant and affordable bathroom renovation in less time and for less money.</p>
        </div>
      </div>
    </div>
    
  </div>
</section>
<!--3services section starts here -->
<section id="service-section">
  <div class="container mt-med-padding">
    <div class="row">
      <?php 
      foreach ($course as $courses){

      ?>
      <div class="col-md-3 col-sm-3 col-xs-12">
        <div class="service-content">
          <figure>
           <?php echo '<img src="data:image/jpeg;base64,'.base64_encode($courses['course_image'] ).'" height="200" width="200" class="img-responsive" />';  ?>
            <figcaption>
            <h3 class="srv-title"><?php echo $courses['course_name']; ?></h3>
            <p><?php echo $courses['course_description']; ?></p>
            <a href="service-details.php">Find More</a>
            </figcaption>
          </figure>
        </div>
      </div>
      <?php } ?>
    </div>
  
  </div>
</section>
<!-- why us section starts here -->
<section id="why-us">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-7">
        <div class="why-us-wrapper">
          <h2>Why Choose Us?</h2>
          <div class="mt-wrapper">
            <div class="mt-section">
              <div class="row">
                <div class="col-md-3 col-sm-2 col-xs-2">
                  <img src="images/ico1.png">
                </div>
                <div class="col-md-9 col-sm-10 col-xs-10">
                  <h3>Free Consulation and Quote</h3>
                  <p>If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this fre</p>
                </div>
              </div>
            </div>
            <!-- mt-section1 ends here -->
            <div class="mt-section">
              <div class="row">
                <div class="col-md-3 col-sm-2 col-xs-2">
                  <img src="images/ico2.png">
                </div>
                <div class="col-md-9 col-sm-10 col-xs-10">
                  <h3>Fully Licensed and Insured</h3>
                  <p>If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this fre</p>
                </div>
              </div>
            </div>
            <!-- mt-section2 ends here -->
            <div class="mt-section">
              <div class="row">
                <div class="col-md-3 col-sm-2 col-xs-2">
                  <img src="images/ico3.png">
                </div>
                <div class="col-md-9 col-sm-10 col-xs-10">
                  <h3>Expert Design and Planning</h3>
                  <p>If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this fre</p>
                </div>
              </div>
            </div>
          <!-- mt-section3 ends here -->

            <div class="mt-section">
            <a href="why-us.php">+Find More Reasons</a>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- gallery section starts here -->
<section id="gallery-section">
  <div class="container">
    <div class="gal-intro">
      <h3 class="gal-title">Custom works completed by is at various locations</h3>
      <p>Please click on the image below to enlarge</p>
    </div>
    <div class="gallery-dynamo">
      <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-4 thumbnail">
          <a class="lightbox" href="images/gallery/w1_lg.jpg">
            <img src="images/gallery/w1_sm.jpg"/>
          </a>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-4 thumbnail">
          <a class="lightbox" href="images/gallery/w2_lg.jpg">
            <img src="images/gallery/w2_sm.jpg"/>
          </a>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-4 thumbnail">
         <a class="lightbox" href="images/gallery/w3_lg.jpg">
            <img src="images/gallery/w3_sm.jpg"/>
          </a>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-4 thumbnail">
          <a class="lightbox" href="images/gallery/w4_lg.jpg">
            <img src="images/gallery/w4_sm.jpg"/>
          </a>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-4 thumbnail">
          <a class="lightbox" href="images/gallery/w5_lg.jpg">
            <img src="images/gallery/w5_sm.jpg"/>
          </a>
        </div>
        
        <div class="col-md-3 col-sm-3 col-xs-4 thumbnail">
          <a class="lightbox" href="images/gallery/w6_lg.jpg">
            <img src="images/gallery/w6_sm.jpg"/>
          </a>
        </div>
        
        <div class="col-md-3 col-sm-3 col-xs-6 thumbnail">
        <a class="lightbox" href="images/gallery/w7_lg.jpg">
            <img src="images/gallery/w7_sm.jpg"/>
          </a>
        </div>
        
        <div class="col-md-3 col-sm-3 col-xs-6 thumbnail">
           <a class="lightbox" href="images/gallery/w8_lg.jpg">
            <img src="images/gallery/w8_sm.jpg"/>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- contact form section starts here  -->
<section id="contact-section">
  <div class="container mt-med-padding">
    <div class="contact-intro text-center">
      <h3>Get Your Renovation Sorted Today With A FREE Onsite Quote</h3>
      <p>Please fill in the form below and we will get in touch.</p>
    </div>
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <form class="contact-form">
          <div class="form-group">
            <label class="sr-only" >Name</label>
            <input type="text" class="form-control"  placeholder="Your Name">
          </div>
                    <div class="form-group">
            <label class="sr-only">Phone</label>
            <input type="text" class="form-control"  placeholder="Phone">
          </div>
          <div class="form-group">
            <label class="sr-only">Email</label>
            <input type="Email" class="form-control"  placeholder="Email">
          </div>
                    <div class="form-group">
            <label class="sr-only">Suburb</label>
            <input type="text" class="form-control"  placeholder="Suburb">
          </div>
          <div class="form-group">
            <label class="sr-only"> Question/Message </label>
            <textarea class="form-control" rows="5" placeholder="Message:"></textarea>
          </div>
          <input id="submit"  class="mt-btn" type="submit" value="SEND NOW">
        </form>
      </div>
    </div>
  </div>
</section>
<!-- bathroom renovaton section starts here -->
<section id="bath-renovation">
  <div class="container mt-lg-padding">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="ren-section">
          <h2>Stress Free Bathroom Renovations Sydney</h2>
          <h3>From Concept to Completion</h3>
          <p>If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this free psd on the side of the logo or shape above, it does not look good .If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this free psd on the side of the logo or shape above, it does not look good.</p>
          <p>If I put the name of this free psd on the side of the logo or shape above, it does not look good..If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this free psd on the side of the logo or shape above, it does not look good .If I put the name of this free psd on the side of the logo or shape above, it does not look good. If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this free psd on the side of the logo or shape above, it does not look good If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this free psd on the side of the logo or shape above, it does not look good .</p>
          <p>If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this free psd on the side of the logo or shape above, it does not look good..If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this free psd on the side of the logo or shape above, it does not look good.</p>
          <p>If I put the name of this free psd on the side of the logo or shape above, it does not look good .If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this free psd on the side of the logo or shape above, it does not look good If I put the name of this free psd on the side of the logo or shape above, it does not look good.If I put the name of this free psd on the side of the logo or shape above, it does not look good. </div>
        
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="call-section">
          <div class="call-content">
          <h3>Call now for friendly advice</h3>
           <div class="phone"> <a href="tel:+0 123 456 789"><span class="phone-icon"><img src="images/ico_phone.png" alt="" ></span>Call us on 0408 123 676</a>  </div>
         </div>
        </div>
      </div>
    </div>
  </div>
</section>
