<?php
// foreach ($user as $u) 
// {
// 	echo $u['user_id'];
// 	echo '</br>'. $u['username'];
// 	echo '<br>'. $u['fullname'];
// }
?>


<?php
$menuSelected = 'home';
 ?>
<!--banner-container-starts-here-->
<section id="billboard-container">
  <div id="billboard-sliders">
    <div class="item">
      <figure>
        <img src="<?php echo base_url('assets/images/banner2.jpg')?>" alt="" />
      </figure>
      <div class="container">
        <div class="banner-caption">
          <h1>Growing Compassionate young leaders through service Learning</h1>
          <p>A free oline course platform</p>
        </div>
      </div>
    </div>
    <div class="item">
      <figure>
        <img src="<?php echo base_url('assets/images/banner1.jpg')?>" alt="" />
      </figure>
      <div class="container">
        <div class="banner-caption">
           <h1>Growing Compassionate young leaders through service Learning</h1>
          <p>A free oline course platform</p>d for less money.</p>
        </div>
      </div>
    </div>
    <div class="item">
      <figure>
        <img src="<?php echo base_url('assets/images/banner3.jpg')?>" alt="" />
      </figure>
      <div class="container">
        <div class="banner-caption">
          <h1>Custom Quality Bathroom Renovations in Sydney</h1>
          <p>Elegant and affordable bathroom renovation in less time and for less money.</p>
        </div>
      </div>
    </div>
    
  </div>
</section>
<!--3services section starts here -->
<section id="service-section">
  <div class="container mt-med-padding">
    <div class="row">
      <?php 
      foreach ($course as $courses){

      ?>
      <div class="col-md-3 col-sm-3 col-xs-12">
        <div class="service-content">
          <figure>
           <?php echo '<img src="data:image/jpeg;base64,'.base64_encode($courses['course_image'] ).'" height="200" width="200" class="img-responsive" />';  ?>
            <figcaption>
            <h3 class="srv-title"><?php echo $courses['course_name']; ?></h3>
            <p><?php echo $courses['course_description']; ?></p>
            <a href="service-details.php">Find More</a>
            </figcaption>
          </figure>
        </div>
      </div>
      <?php } ?>
    </div>
  
  </div>
</section>
<!-- why us section starts here -->
