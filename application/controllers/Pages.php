<?php 


class Pages extends CI_Controller {

public function index()
	{
		$this->load->model('Course_model');
		$data['course']= $this->Course_model->get_all_course();
		$this->load->view('includes/header');
		$this->load->view('front-page', $data);
		$this->load->view('includes/footer');
	}


	public function membership()
	{
		$this->load->view('includes/header');
		$this->load->view('pages/membership');
		$this->load->view('includes/footer');
	}

	public function userDashboard()
	{
	$this->load->model('Course_model');
    $data['materials']= $this->Course_model->get_course_materials();
    $this->load->view('includes/header');
    $this->load->view('pages/userDashboard', $data);
    //$this->load->view('admin/courselist');
    $this->load->view('includes/footer');
	}

	public function adminDashboard()
	{
		$this->load->view('includes/admin_header');
		$this->load->view('includes/sidebar');
		$this->load->view('admin/dashboard');
		$this->load->view('includes/admin_footer');
	} 




	}