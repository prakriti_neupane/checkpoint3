<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('Course_model');
	}

	public function add()
	{
		$this->load->view('category/add');
	}

	public function index()
	{
		$this->load->model('Course_model');
		$data['course']= $this->Course_model->get_all_course();
		$this->load->view('includes/header');
		$this->load->view('course/index', $data);
		$this->load->view('includes/footer');
	}



	public function addCourses()
	{
		$this->load->view('includes/admin_header');
		$this->load->view('includes/sidebar');
    	$this->load->view('admin/addCourse');
    	$this->load->view('includes/admin_footer');
	}


	public function listCourses()
	{
		//$this->load->model('Course_model');
		$data['course']= $this->Course_model->get_all_course();
		$this->load->view('includes/admin_header');
		$this->load->view('includes/sidebar');
		$this->load->view('admin/courselist', $data);
		$this->load->view('includes/admin_footer');
	}


	public function deleteCourse($id){
			//$this->load->model('Course_model');
			$result= $this->Course_model->removeCourses($id);
			if($result){
				echo "deleted";
	   // 	$this->session->set_flashdata('eventSuccess_msg', 'Record deleted successfully');
	   // }else{
	   // 	$this->session->set_flashdata('eventError_msg', 'Fail to delete record');
	   // }
				}else{
					echo "error";
				}
		redirect('course/listCourses');
			
		}



}
